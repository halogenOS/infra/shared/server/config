
pushd() {
	command pushd "$1" >/dev/null || return $?
}

popd() {
	command popd "$1" >/dev/null || return $?
}

msg() {
	[ ! -z "$current_stage" ] && echo -en "\x1b[38;2;0;229;255m[$current_stage]\033[0m "
	echo -e "\e[1m$@\e[0m"
}

errmsg() {
	[ ! -z "$current_stage" ] && echo -en "\x1b[38;2;244;67;54m[$current_stage]\033[0m "
	echo -e "\e[1m$@\e[0m"
}

create_backup_file() {
	[ -f "$1" ] && cp "$1" "$1_$(date +'%d%m%Y_%H%M%S_%s').bak" || msg "$1 does not exist, skipping backup"
}

