
setup_user_sudoers_file() {
	echo "/etc/sudoers.d/user_$new_user"
}

setup_user_internal() {
	msg "Creating new user..."
	new_user=$(pwgen -A0sB 6 1)
	msg "User name: $new_user"
	useradd -m -s /bin/zsh $new_user
	msg "Temporarily granting nopasswd sudo..."
	echo "$new_user ALL=(ALL) NOPASSWD: ALL" > $(setup_user_sudoers_file)
	msg "Running hooks..."
	before_user_switch
	msg "Setting up user..."
	su $new_user -c "bash $local_path/setup_user_unpriv.sh"
	msg "Setting password-requiring sudo grant..."
	echo "$new_user ALL=(ALL) ALL" > $(setup_user_sudoers_file)

	msg "You will now be passed on to the passwd utility to set a password"
	msg "Please use a secure password – either use the 'pwgen' utility or enter one that's hard to guess but easy to remember"
	msg "You can do the honors now"
	echo

	while ! passwd $new_user; do
		msg "Oops, looks like you messed that one up. Let's give that another try."
	done

	msg "Great, now save it in a password manager (for example 'gopass') or memorize it."
	msg "Username: $new_user"
	msg
	msg "Press [ENTER] to continue"
	read
}

setup_user() {
	setup_user_internal || (
	errmsg "Failed setting up user, rolling back changes..."
	userdel -fr $new_user || :
	rm -f $(setup_user_sudoers_file)
	)
	false
}
