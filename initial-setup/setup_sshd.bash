ssh_port=""
old_sshd_config="/etc/ssh/sshd_config_$(date +'%d%m%Y_%H%M%S_%s')"
sshd_config="/etc/ssh/sshd_config"

setup_sshd_internal() {
	set -e

	[ -z "$new_user" ] && errmsg "new_user not set!" && return 1

	msg "Setting up OpenSSH Server..."

	msg "Backing up current configuration..."

	create_backup_file "$sshd_config"

	msg "Generating new port number..."
	# generates a number between 0 and 1024
	# 177777 is the biggest number od -An -N 2 can print
	# test it with 'printf "\xff\xff" | od -An -N 2'
	local random_port=$(perl -E "use POSIX; say floor($(head -c 2 /dev/random | od -An -N 2)/177777*1024)")
	ssh_port="$random_port"

	msg "SSH port: $random_port"

	msg "Creating new configuration..."

	cat <<EOF > $sshd_config
# This configuration was generated

Port $random_port

HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

LoginGraceTime 1m
PermitRootLogin no
StrictModes yes
MaxAuthTries 3
MaxSessions 8

# From the official arch package:
# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile .ssh/authorized_keys

# Disable password authentication
PasswordAuthentication no
ChallengeResponseAuthentication no

UsePAM yes

AllowAgentForwarding no
AllowTcpForwarding no
AcceptEnv COLORTERM TERM
Gatewayports no
X11Forwarding no
PrintMotd no # pam does that
TCPKeepAlive yes
PermitUserEnvironment no
PermitTunnel no

AllowUsers $new_user

EOF

	msg "New configuration written."

	msg "You will need to provide your SSH public key now, otherwise you'll lose access to this server"
	local ssh_key=""
	while [ -z "$ssh_key" ]; do
		read -p "SSH-Key: " ssh_key
		if ! echo -n "$ssh_key" | ssh-keygen -l -f - 2>&1 >/dev/null; then
			errmsg "SSH-Key is invalid, try again."
			ssh_key=""
		else
			msg "Your key: $(echo -n "$ssh_key" | ssh-keygen -l -f -)"
			local it_is_correct=false
			read -p "Is this correct? [y/N]: " is_it_correct_input
			case "$is_it_correct_input" in
				[Yy]) it_is_correct=true ;;
			esac
			if ! $it_is_correct; then
				ssh_key=""
			else
				msg "Key will be used for user $new_user"
			fi

		fi
	done

	msg "Setting up $new_user's .ssh dir..."
	su $new_user -c 'bash -c '"'"'mkdir -p $HOME/.ssh && chmod 700 $HOME/.ssh && install -b -m 600 /dev/null $HOME/.ssh/authorized_keys'"'"''

	msg "Adding key to authorized keys of $new_user..."
	echo "$ssh_key" | su $new_user -c 'bash -c '"'"'cat >> $HOME/.ssh/authorized_keys'"'"''

	msg "Restarting sshd..."
	systemctl restart sshd
}

setup_sshd() {
	setup_sshd_internal || (
	errmsg "SSH setup failed, rolling back changes..."
	[ -f "$old_sshd_config" ] && mv $old_sshd_config $sshd_config || rm -f $sshd_config
	systemctl restart sshd
	false
	)
}
