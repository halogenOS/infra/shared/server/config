# see https://wiki.archlinux.org/index.php/Security

secure_server() {
	msg "Setting up pam_pwquality..."
	cat <<EOF > /etc/pam.d/passwd
#%PAM-1.0
password required pam_pwquality.so retry=2 minlen=12 difok=12 dcredit=-2 ucredit=-2 ocredit=-2 lcredit=-2 minclass=2 maxrepeat=2 reject_username
password required pam_unix.so use_authtok sha512 shadow
EOF

	msg "Applying strict umask rule..."
	! grep "umask 077" >/dev/null /etc/profile && echo "umask 077" >> /etc/profile

	msg "Applying delay after failed login attempt..."
	echo "auth optional pam_faildelay.so delay=4000000" >> /etc/pam.d/system-login

	msg "Applying user lockout after failed login attempts..."
	echo "auth required pam_tally2.so deny=3 unlock_time=120 onerr=succeed file=/var/log/tallylog" >> /etc/pam.d/system-login

	msg "Disabling root account..."
	passwd --lock root

	msg "Installing AppArmor..."
	pacman -S --noconfirm --needed apparmor

	msg "Enabling AppArmor..."
	systemctl enable apparmor

	msg "Setting kernel cmdline..."
	sed -i -re 's/GRUB_CMDLINE_LINUX_DEFAULT="(.*)"/GRUB_CMDLINE_LINUX_DEFAULT="\1 apparmor=1 lsm=capability,lockdown,yama,apparmor security=apparmor lockdown=confidentiality l1tf=full,force mds=full,nosmt mitigations=auto,nosmt nosmt=force"/' /etc/default/grub

	msg "Regenerating GRUB config..."
	grub-mkconfig > /boot/grub/grub.cfg

	msg "Applying restriction to kernel logs..."
	echo -n "kernel.dmesg_restrict = 1" > /etc/sysctl.d/51-dmesg-restrict.conf

	msg "Applying kernel pointer access restriction..."
	echo -n "kernel.kptr_restrict = 2" > /etc/sysctl.d/51-kptr-restrict.conf

	msg "Disabling kexec..."
	echo -n "kernel.kexec_load_disabled = 1" > /etc/sysctl.d/51-kexec-restrict.conf

	msg "Hardening TCP/IP..."
	cat <<EOF > /etc/sysctl.d/52-tcpip-hardening.conf
net.ipv4.tcp_syncookies = 1
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
EOF

	msg "Reboot the server now"
}

