#!/bin/bash -e

if hash yay >/dev/null 2>/dev/null; then
	echo "yay is already installed"
	yay --version
	exit 0
fi

pushd /tmp
rm -rf yay
echo "Cloning yay..."
git clone https://aur.archlinux.org/yay.git
cd yay
echo "Installing yay..."
makepkg -si --noconfirm
echo "Cleaning up..."
rm -rf yay
popd

