
iptables_rules="/etc/iptables/iptables.rules"
ip6tables_rules="/etc/iptables/ip6tables.rules"

setup_firewall() {
	msg "Configuring IPv4 firewall..."
	cat <<EOF > $iptables_rules
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:LOG_DROP - [0:0]
:LOG_REJECT - [0:0]
:inchain - [0:0]
:outchain - [0:0]
-A INPUT -j inchain
-A OUTPUT -j outchain
-A LOG_DROP -j LOG --log-prefix "The fire burned: " --log-level 7
-A LOG_DROP -j DROP
-A LOG_REJECT -j LOG --log-prefix "Retoure for: " --log-level 7
-A LOG_REJECT -j REJECT --reject-with icmp-port-unreachable
-A inchain -i lo -j ACCEPT
-A inchain -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A inchain -p tcp -m tcp --dport 443 -j ACCEPT
-A inchain -p tcp -m tcp --dport 80 -j ACCEPT
-A inchain -p tcp -m tcp --dport $ssh_port -j ACCEPT
-A inchain -p icmp --icmp-type 8 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
-A INPUT -j LOG_REJECT
COMMIT
EOF

	msg "Configuring IPv6 firewall..."
	cat <<EOF > $ip6tables_rules
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:LOG_DROP - [0:0]
:LOG_REJECT - [0:0]
:inchain - [0:0]
:outchain - [0:0]
-A INPUT -j inchain
-A OUTPUT -j outchain
-A LOG_DROP -j LOG --log-prefix "The fire burned v6: " --log-level 7
-A LOG_DROP -j DROP
-A LOG_REJECT -j LOG --log-prefix "Retoure for v6: " --log-level 7
-A LOG_REJECT -j REJECT
-A inchain -i lo -j ACCEPT
-A inchain -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A inchain -p tcp -m tcp --dport 443 -j ACCEPT
-A inchain -p tcp -m tcp --dport 80 -j ACCEPT
-A inchain -p tcp -m tcp --dport $ssh_port -j ACCEPT
-A INPUT -j LOG_REJECT
COMMIT
EOF

	msg "Enabling firewall..."
	systemctl enable --now iptables ip6tables

	msg "Firewall is now configured."
}
