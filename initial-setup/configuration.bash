PACKAGES_TO_INSTALL=(
	curl wget bash-completion git docker
	inetutils iputils dnsutils haveged
	iptables btrfs-progs xfsprogs neovim
	at cronie tmux zsh zsh-completions
	pwgen base-devel ed sed grep awk perl
	openssh docker-compose libpwquality lvm2
)

SERVICES_TO_ENABLE=(
	haveged cronie atd docker
)

RUNNABLE_STAGES=(
	update_system pre_install_packages install_packages
	post_install_packages basic_configuration default_stages
	after_first_reboot setup_user setup_sshd secure_server
	setup_firewall test install_yay setup_fail2ban
)
