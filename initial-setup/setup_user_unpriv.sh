#!/bin/bash -e

echo "Setting up zsh..."
curl 'https://gitlab.com/xdevs23/ah-yes-zsh/-/raw/master/install.sh' | bash -e

echo "Applying customizations..."
cat <<EOF > ~/.customizedrc
if [[ \$COLORTERM =~ ^(truecolor|24bit)$ ]]; then
typeset -g POWERLEVEL9K_CUSTOM_OS_ICON_BACKGROUND="#1B5E20"
typeset -g POWERLEVEL9K_HOST_BACKGROUND="#2E7D32"
typeset -g POWERLEVEL9K_USER_BACKGROUND="#388E3C"
typeset -g POWERLEVEL9K_DIR_BACKGROUND="#43A047"
typeset -g POWERLEVEL9K_DIR_ANCHOR_BACKGROUND="#4CAF50"

typeset -g POWERLEVEL9K_CUSTOM_OS_ICON_FOREGROUND="#ffffff"
typeset -g POWERLEVEL9K_HOST_FOREGROUND="#ffffff"
typeset -g POWERLEVEL9K_USER_FOREGROUND="#ffffff"
typeset -g POWERLEVEL9K_DIR_FOREGROUND="#ffffff"
typeset -g POWERLEVEL9K_DIR_ANCHOR_FOREGROUND="#fefefe"
fi

DISABLE_UPDATE_PROMPT="false"
EOF


echo "User is set up"

