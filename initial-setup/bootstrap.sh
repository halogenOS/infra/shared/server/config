#!/bin/bash -e

set -e

local_path=`dirname "$(realpath "$0")"`
pushd "$local_path" >/dev/null

source configuration.bash
source util.bash

echo
echo "Server setup script"
echo

current_stage=""
new_user=$(grep -E '.+?:.+?:1000:1000::.*' /etc/passwd  | cut -d ':' -f1 || :)
ssh_port=$(grep 'Port .*' /etc/ssh/sshd_config | cut -d ' ' -f2 || :)

run_stage() {
	local stage="$1"
	local found=false
	for runnable_stage in ${RUNNABLE_STAGES[*]}; do
		[ "$runnable_stage" == "$stage" ] && found=true && break
	done
	! $found && echo "Stage '$stage' is not runnable, choose one of: ${RUNNABLE_STAGES[*]}" && exit 1
	if [ -f $stage.bash ]; then
		source $stage.bash
	fi

	[ "$stage" != "test" ] && [ `whoami` != "root" ] && (
	echo "Please run this as root."
	exit 1
	)

	current_stage="$stage"
	$stage
	current_stage=""
}

test() {
	bash -n "$0"
	msg "It works!"
}

update_system() {
	msg "Updating system..."
	pacman -Syu --noconfirm
}

pre_install_packages() {
	msg "Running pre-install tasks..."	
}

install_packages() {
	msg "Installing packages..."
	pacman -S --needed --noconfirm ${PACKAGES_TO_INSTALL[*]}
}

post_install_packages() {
	msg "Running post-install tasks..."

	msg "Enabling services..."
	# Enable some services
	systemctl enable --now ${SERVICES_TO_ENABLE[*]}

	msg "Installing kernel..."
	# Install correct kernel
	pacman -S --noconfirm linux
	pacman -R --noconfirm linux-lts || :

	msg "Regenerating GRUB config..."
	grub-mkconfig > /boot/grub/grub.cfg
}

basic_configuration() {
	msg "Basic configuration"

	# Ask for hostname
	local domain=""
	while [ -z "$domain" ]; do
		read -p "Domain of this server: " domain
	done
	local sld=`echo "$domain" | cut -d '.' -f-1`

	local server_no=""
	while [ -z "$server_no" ]; do
		echo -n "Server number: "
		read server_no
	done

	local is_public=""
	local is_public_input=""
	while
		case $is_public_input in
			[Yy]) is_public=true ;;
			[Nn]) is_public=false ;;
			*) ;;
		esac
		[ -z "$is_public_input" ]
	do read -p "Is this server public? [y/n]: " is_public_input
	done

	local is_public_val="pub"
	$is_public || is_public_val="priv"

	echo
	echo "Domain: $domain"
	echo "Second-Level-Domain: $sld"
	echo "Server no.: $server_no"
	echo "Is public: $($is_public && echo "Yes" || echo "No")"
	echo

	local is_correct=false
	local is_correct_input=""
	read -p "Is this information correct? [y/N]: " is_correct_input
	case "$is_correct_input" in
		[Yy]) is_correct=true ;;
		*) ;;
	esac
	$is_correct && msg "Alright, proceeding with configuration"

	if ! $is_correct; then
		echo "You said $is_correct_input, re-enter the information"
		basic_configuration
		return $?
	fi

	msg "Writing hosts file..."

	cat <<EOF > /etc/hosts
# Static table lookup for hostnames.
# See hosts(5) for details.

#<ip-address>	<hostname.domain.org>	<hostname>
127.0.0.1	srv${server_no}-${is_public_val}.${domain}	localhost
::1		srv${server_no}-${is_public_val}.${domain}	localhost
127.0.1.1	${sld}-srv${server_no}-${is_public_val}.localdomain	${sld}-srv${server_no}-${is_public_val}
EOF


	msg "Setting hostname..."
	echo "$domain" > /etc/hostname

	msg "Setting defaults..."
	cat <<EOF >> /etc/profile

# Added automatically
export EDITOR="/usr/bin/vim"
EOF

	msg "Basic configuration is done, restart the server now."
}

before_user_switch() {
	run_stage install_yay
}

default_stages() {
	run_stage update_system
	run_stage pre_install_packages
	run_stage install_packages
	run_stage post_install_packages
	run_stage basic_configuration
	msg "Continue configuration with $0 after_first_reboot"
}

after_first_reboot() {
	run_stage setup_user
	run_stage setup_sshd
	run_stage secure_server
	run_stage setup_firewall
	run_stage setup_fail2ban
}

if [ -z "$1" ]; then
	run_stage default_stages
else
	run_stage "$1"
fi

popd

