
setup_fail2ban() {
	msg "Installing fail2ban..."
	pacman -S --noconfirm --needed fail2ban

	msg "Enabling and starting fail2ban..."
	systemctl enable --now fail2ban

	msg "Done."
}
